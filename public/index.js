var room = 0;
var global_txt;
var message_num = 0;
var user_email = '';
var now_room = '';
var now_user_room_total;
var room_list_total;
var temp;
var total_room = [];
var first_count_room = 0;
var second_count_room = 0;
// The html code for post
var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
var str_after_content = "</p></div></div>\n";
var ref;
var string_before_button = "<button  class='btn btn-lg btn-block roombtn' style='height:70px; font-size:30px' onclick='chooseroom(&quot;";
var string_in_button = "&quot;)'>";
var string_after_button = "</button>\n";
var invite_email;
var global_txt_invite;
var flag;
var invite_ref_global;
var childshot_key;
var notification_;

function init() {

    var lobby = document.getElementById('lobby');

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    alert("User sign out success!");
                    window.location.href = "signin.html";
                }).catch(function (error) {
                    alert("User sign out failed!");
                })
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    // com_list 資料的寫入

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {

            var Ref = firebase.database().ref("room_list/room");

            Ref.once('value', (function (data) {
                data.forEach(function (childshot) {
                    if (childshot.val().name == now_room) {
                        var message = firebase.database().ref("room_list/room/" + childshot.key + "/message");
                        var data = {
                            data: post_txt.value.replace(/</g,"&lt;").replace(/>/g,"&gt;"),
                            email: user_email
                        };
                        message.push(data);

                        post_txt.value = "";
                    }
                });
            }))
        }
    });

    // 大廳

    lobby.addEventListener('click', function () {
        now_room = "lobby";

        //

        var total_post = [];
        var first_count = 0;
        var second_count = 0;

        var Ref = firebase.database().ref("room_list/room");
        Ref.once('value')
            .then(function (data) {
                data.forEach(function (childshot) {
                    if (childshot.val().name == now_room) {
                        ref = firebase.database().ref("room_list/room/" + childshot.key + "/message");
                        ref.once('value', (function (data_) {
                            data_.forEach(function (msg) {
                                var childData = msg.val();
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                                first_count += 1;
                            })

                            document.getElementById('post_list').innerHTML = total_post.join('');
        
                            ref.on('child_added', function (snapshot) {
                                second_count += 1;
                                if (second_count > first_count) {
                                    var childData = snapshot.val();
                                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                                    document.getElementById('post_list').innerHTML = total_post.join('');
                                }
                            });
                        }))
                    }
                });
    
            })
            .catch(e => console.log(e.message));
    });


    // room_button history

    var room_button_history = firebase.database().ref('people_list/user');

    room_button_history.once('value', (function (data) {
        data.forEach(function (childshot) {
            if (childshot.val().email == user_email) {
                var room = firebase.database().ref('people_list/user/' + childshot.key + '/room');

                room.once('value', (function (data) {
                    data.forEach(function (childshot) {
                        var childData = childshot.val();

                        
                        // 建房間
                        var new_room = document.createElement('input');
                        new_room.type = 'button';
                        new_room.value = childData.name;
                        new_room.id = "new_room";
                        new_room.addEventListener('click', function() {
                        now_room = childData.name;
                        // 歷史資料

                        var total_post = [];
                        var first_count = 0;
                        var second_count = 0;

                        var Ref = firebase.database().ref("room_list/room");
                        Ref.once('value')
                            .then(function (data) {
                                data.forEach(function (childshot) {
                                    if (childshot.val().name == now_room) {
                                        ref = firebase.database().ref("room_list/room/" + childshot.key + "/message");
                                        ref.once('value', (function (data_) {
                                            data_.forEach(function (msg) {
                                               var childData = msg.val();
                                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                                                first_count += 1;
                                            })

                                            document.getElementById('post_list').innerHTML = total_post.join('');
        
                                            ref.on('child_added', function (snapshot) {
                                                second_count += 1;
                                                if (second_count > first_count) {
                                                    var childData = snapshot.val();
                                                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                                                    document.getElementById('post_list').innerHTML = total_post.join('');
                                                }
                                            });
                                        }))
                                    }
                                });

                            })
                            .catch(e => console.log(e.message));
                        })
                        document.getElementById('room_list').appendChild(new_room);

                    });
                }))
            }
        });
    }))

    // invite

    var invite = document.getElementById('invite');

    invite.addEventListener('click', function () {
        textarea_onclick_invite();
    });


    // 新增一個房間
    var create = document.getElementById('create');

    create.addEventListener('click', function () {
        textarea_onclick();
    });

}

// 新增文字框

function invite_block() {
    flag = 1;
    var invite = firebase.database().ref('people_list/user');
    invite.once('value', (function (data) {
        data.forEach(function (childshot) {
            if (childshot.val().email == global_txt_invite.value) {
                invite_ref_global = firebase.database().ref('people_list/user/' + childshot.key + '/room');
                childshot_key = childshot.key; // global記住
                invite_ref_global.once('value', (function (data) {
                    now_user_room_total = data.numChildren();
                    data.forEach(function (childshot) {
                        if (childshot.val().name == now_room) {
                            flag = 0;
                        }
                    });
                })).then(function() {
                    if (flag == 0) {
                        alert("Already in.");
                    }
                    else {
                        var room_add = firebase.database().ref('people_list/user/' + childshot_key + '/room/room' + now_user_room_total);
                        var data = {
                            name: now_room
                        }
                        room_add.update(data);
                        alert("Welcome to our room!");
                    }
                })
            }
        });
    }))
}

function room_history() {

    // 建房間
    var new_room = document.createElement('input');
    new_room.type = 'button';
    new_room.value = global_txt.value;
    new_room.id = "new_room";
    new_room.addEventListener('click', function() {
    now_room = global_txt.value;
    // 歷史資料

    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    var Ref = firebase.database().ref("room_list/room");
    Ref.once('value')
        .then(function (data) {
            data.forEach(function (childshot) {
                if (childshot.val().name == now_room) {
                    ref = firebase.database().ref("room_list/room/" + childshot.key + "/message");
                    ref.once('value', (function (data_) {
                        data_.forEach(function (msg) {
                            var childData = msg.val();
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                            first_count += 1;
                        })

                        document.getElementById('post_list').innerHTML = total_post.join('');
        
                        ref.on('child_added', function (snapshot) {
                            second_count += 1;
                            if (second_count > first_count) {
                                var childData = snapshot.val();
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                                document.getElementById('post_list').innerHTML = total_post.join('');
                            }
                        });
                    }))
                }
            });

        })
        .catch(e => console.log(e.message));
    })
    document.getElementById('room_list').appendChild(new_room);
    document.getElementById('temp').removeChild(this);
}

function textarea_onclick() {

    var txt = document.createElement("input");
    global_txt = txt;
    global_txt.type = "text";

    global_txt.style.zIndex = 3;
    global_txt.style.width = '200px';
    global_txt.style.left = '10%';
    global_txt.style.top = '25%';


    document.getElementById('temp').appendChild(global_txt);
    txt.onkeydown = handleEnter;

    txt.focus();
}

function textarea_onclick_invite() {

    var txt = document.createElement("input");
    global_txt_invite = txt;
    global_txt_invite.type = "text";
    global_txt_invite.style.zIndex = 3;
    global_txt_invite.style.width = '200px';
    global_txt_invite.style.left = '30%';
    global_txt_invite.style.top = '40%';



    document.getElementById('temp1').appendChild(global_txt_invite);
    txt.onkeydown = handleEnter_invite;

    txt.focus();
}

// 按enter消掉文字框，並且創一個button掛在room_list上，寫入firebase

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {

        // 建房間
        var new_room = document.createElement('input');
        new_room.type = 'button';
        new_room.value = global_txt.value;
        new_room.id = "new_room";
        new_room.addEventListener('click', function() {
            now_room = global_txt.value;
            // 歷史資料

            var total_post = [];
            var first_count = 0;
            var second_count = 0;

            

            var Ref = firebase.database().ref("room_list/room");
            Ref.once('value')
                .then(function (data) {
                    data.forEach(function (childshot) {
                        if (childshot.val().name == now_room) {
                            ref = firebase.database().ref("room_list/room/" + childshot.key + "/message");
                            ref.once('value', (function (data_) {
                                data_.forEach(function (msg) {
                                    var childData = msg.val();
                                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                                    first_count += 1;
                                })

                                document.getElementById('post_list').innerHTML = total_post.join('');
            
                                ref.on('child_added', function (snapshot) {
                                    second_count += 1;
                                    if (second_count > first_count) {
                                        var childData = snapshot.val();
                                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                                        document.getElementById('post_list').innerHTML = total_post.join('');
                                    }
                                });
                            }))
                        }
                        else 
                            firebase.database().ref("room_list/room/" + childshot.key + "/message").off();
                    });
        
                })
                .catch(e => console.log(e.message));
        })


        document.getElementById('room_list').appendChild(new_room);
        document.getElementById('temp').removeChild(this);

        // 寫入firebase

        var room_add = firebase.database().ref('room_list/room');


        room_add.once('value', (function (data) {
            room_list_total = data.numChildren();

        })).then(function () {
            var room_add = firebase.database().ref('room_list/room/room' + room_list_total);
            var data = {
                name: global_txt.value
            }
            room_add.update(data);
        })


        // 新增房間到user的list下


        var EmailtoRoom = firebase.database().ref('people_list/user');


        EmailtoRoom.once('value', (function (data) {
            data.forEach(function (childshot) {
                if (childshot.val().email == user_email) {
                    var room = firebase.database().ref('people_list/user/' + childshot.key + '/room');

                    room.once('value', (function (data) {
                        now_user_room_total = data.numChildren();

                    })).then(function () {
                        var room_add = firebase.database().ref('people_list/user/' + childshot.key + '/room/room' + now_user_room_total);
                        var data = {
                            name: global_txt.value
                        }
                        room_add.update(data);
                    })
                }
            });
        }))

    }
}

function handleEnter_invite(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {

        document.getElementById('temp1').removeChild(this);
        invite_block();
    }
}

function notifyMe() {

    if (!("Notification" in window)) {
      alert("這個瀏覽器不支援 Notification");
    }
  

    else if (Notification.permission === "granted") {

      var notification = new Notification(notification_);
    }
  

    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        if (permission === "granted") {
          var notification = new Notification("歡迎使用通知");
        }
      });
    }
  
  }


window.onload = function () {
    init();
};

/* 最初版本的歷史資料

var postsRef = firebase.database().ref('com_list');
// List for store posts html
var total_post = [];
var first_count = 0;
var second_count = 0;

postsRef.once('value')
    .then(function(snapshot) {
       snapshot.forEach(function(childshot) {
            var childData = childshot.val();
            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
            first_count += 1;
        });


        document.getElementById('post_list').innerHTML = total_post.join('');

        postsRef.on('child_added', function(data) {
            second_count += 1;
            if (second_count > first_count) {
                var childData = data.val();
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                document.getElementById('post_list').innerHTML = total_post.join('');
            }
        });


    })
    .catch(e => console.log(e.message));
*/