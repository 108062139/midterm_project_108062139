var user_total = -1;
var temp = 0;
var remake;
var flag;

function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnFacebook = document.getElementById('btnfacebook');

    var Ref = firebase.database().ref('people_list/user');

    btnLogin.addEventListener('click', function() {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()" and clean input field

        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(result) {
            window.location.href = "index.html";
        }).catch(function(error) {
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("error", error);
        });
    });

    btnGoogle.addEventListener('click', function() {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()"
        var provider = new firebase.auth.GoogleAuthProvider();
        // provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        firebase.auth().signInWithPopup(provider).then(function(result) {

            var token = result.credential.accessToken;
            var user = result.user;

            var check = firebase.database().ref('people_list/user');
            flag = 1;
            check.once('value', (function (data) {
                data.forEach(function (childshot) {
                    if (childshot.val().email == user.email) {
                        flag = 0;
                    }
                });
            })).then(function() {
                // 增加usernum 並拿到該值
                if (flag == 0) {
                    alert("歡迎回來 " + user.email);
                    window.location.href = "index.html";
                }
                else {
                    var Ref_usernum = firebase.database().ref('people_list');
                    Ref_usernum.once('value', (function(data) {
                        data.forEach(function(childshot) {
                            if (childshot.key == "user_num") {
                                var childData = childshot.val();
                                if (user_total == -1) user_total = childData.num;
                                user_total = childData.num + 1;
                            }
                        });
                    })).then(function(){
                    
                        // update usernum
                        var Ref_ = firebase.database().ref('people_list/user_num');
                        var data = {
                            num: user_total
                        };
                        Ref_.update(data);
    
                        // 存進firebase
                        var Ref = firebase.database().ref('people_list/user/name' + user_total);
                        var data = {
                            email: user.email
                        };
                        Ref.update(data).then(function(){
                            // 清空帳密欄
                            txtEmail.value = "";
                            txtPassword.value = "";
                            create_alert("success", result);
                        })
                        window.location.href = "index.html";
                    });
                }  
            })
        }).catch(function(error) {
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("error", error);
        });
    });

    btnFacebook.addEventListener('click', function() {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()"
        var provider = new firebase.auth.FacebookAuthProvider();
        // provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        firebase.auth().signInWithPopup(provider).then(function(result) {

            var token = result.credential.accessToken;
            var user = result.user;

            var check = firebase.database().ref('people_list/user');
            flag = 1;
            check.once('value', (function (data) {
                data.forEach(function (childshot) {
                    if (childshot.val().email == user.email) {
                        flag = 0;
                    }
                });
            })).then(function() {
                // 增加usernum 並拿到該值
                if (flag == 0) {
                    alert("歡迎回來 " + user.email);
                    window.location.href = "index.html";
                }
                else {
                    var Ref_usernum = firebase.database().ref('people_list');
                    Ref_usernum.once('value', (function(data) {
                        data.forEach(function(childshot) {
                            if (childshot.key == "user_num") {
                                var childData = childshot.val();
                                if (user_total == -1) user_total = childData.num;
                                user_total = childData.num + 1;
                            }
                        });
                    })).then(function(){
                    
                        // update usernum
                        var Ref_ = firebase.database().ref('people_list/user_num');
                        var data = {
                            num: user_total
                        };
                        Ref_.update(data);
    
                        // 存進firebase
                        var Ref = firebase.database().ref('people_list/user/name' + user_total);
                        var data = {
                            email: user.email
                        };
                        Ref.update(data).then(function(){
                            // 清空帳密欄
                            txtEmail.value = "";
                            txtPassword.value = "";
                            create_alert("success", result);
                        })
                        window.location.href = "index.html";
                    });
                }  
            })
        }).catch(function(error) {
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("error", error);
        });
    });

    btnSignUp.addEventListener('click', function() {
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        console.log(txtEmail.value)
        console.log(txtPassword.value)
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(result) {
            // 增加usernum 並拿到該值
            var Ref_usernum = firebase.database().ref('people_list');
            Ref_usernum.once('value', (function(data) {
                data.forEach(function(childshot) {
                    if (childshot.key == "user_num") {
                        var childData = childshot.val();
                        if (user_total == -1) user_total = childData.num;
                        user_total = childData.num + 1;
                    }
                });
            })).then(function(){
                
                // update usernum
                var Ref_ = firebase.database().ref('people_list/user_num');
                var data = {
                    num: user_total
                };
                Ref_.update(data);

                // 存進firebase
                var Ref = firebase.database().ref('people_list/user/name' + user_total);
                var data = {
                    email: txtEmail.value
                };
                Ref.update(data).then(function(){
                    // 清空帳密欄
                    txtEmail.value = "";
                    txtPassword.value = "";
                    create_alert("success", result);
                });

            });

        }).catch(function(error) {
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("error", error);
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};