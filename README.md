# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm_Project_108062139

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://midterm-af636.web.app

## Website Detail Description
在signin.html登入
要創建一個帳號的話
先在帳密欄打資訊
然後在按最下面的new Account
也可以直接用google, facebook登入
我在登入時會順便把帳號寫入我的firebase裡面
以處理後續的firebase結構
我的firebase結構主要是
user_list跟room_list
user_list管理user
user有room跟name
user的room是為了處理這個使用者有什麼房間
那room_list是在處理所有的房間
room_list的房間下面儲存訊息
所以我一登入進去
就會看到該使用者的所有房間
那當別人把我加進去時
重整頁面
我就會看到別人加我進去的那個房間


# Components Description : 
1. Membership Mechanism : 登入時順便把帳號加到database
2. Host on your Firebase page : firebase deploy
3. Database read/write : 這個部分因為我有user_list跟room_list, user_list的權限都是true, room_list則是需要登入才能看到
4. RWD：做段落式伸縮，排版max-width=多少時就會改
5. Chat room：我登入時，會看到所有房間，而我可以任意的新增房間，並且把該房間添加到room_list下面，訊息也會被儲存到room_list裡面的房間裡，那我新增一個人時，只要讓那個人的所有房間，多一個我邀請他的那個房間，那他就看的到room_list底下的訊息了，房間要點才能看到裡面的訊息
6. Sign Up/In with Google or other third-party accounts：我有做facebook登入
7. Use CSS animation：我signin.html裡面有波動
8. Deal with messages when sending html code：可正確看到html code


# Other Functions Description : 
no

